<div class="breadcrumbs">
  <div class="breadcrumbs-container clearfix">
    
    <div class="lines">
      <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="10px" height="31px" viewBox="0 0 10 31" enable-background="new 0 0 10 31" xml:space="preserve" preserveAspectRatio="none">
        <defs>
          <pattern id="pattern-lines" patternUnits="userSpaceOnUse" x="0" y="0" width="10" height="31">
            <line fill="none" stroke="#010101" stroke-miterlimit="10" x1="0" y1="0.5" x2="10" y2="0.5"/>
            <line fill="none" stroke="#010101" stroke-miterlimit="10" x1="0" y1="9.5" x2="10" y2="9.5"/>
            <line fill="none" stroke="#010101" stroke-miterlimit="10" x1="0" y1="12.5" x2="10" y2="12.5"/>
            <line fill="none" stroke="#010101" stroke-miterlimit="10" x1="0" y1="15.5" x2="10" y2="15.5"/>
            <line fill="none" stroke="#010101" stroke-miterlimit="10" x1="0" y1="18.5" x2="10" y2="18.5"/>
            <line fill="none" stroke="#010101" stroke-miterlimit="10" x1="0" y1="21.5" x2="10" y2="21.5"/>
            <line fill="none" stroke="#010101" stroke-miterlimit="10" x1="0" y1="24.5" x2="10" y2="24.5"/>
            <line fill="none" stroke="#010101" stroke-miterlimit="10" x1="0" y1="27.5" x2="10" y2="27.5"/>
            <line fill="none" stroke="#010101" stroke-miterlimit="10" x1="0" y1="30.5" x2="10" y2="30.5"/>
          </pattern>
        </defs>
        <rect x="0px" y="0px" width="10px" height="31px" style="fill: url(#pattern-lines);" />  
      </svg>
    </div>

    <div class="current-page">%NAME%</div>

  </div>

</div>