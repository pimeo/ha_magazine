<?php
// ARGS FOR PAGERBOARD REQUEST
$args = array(
  'post_type' =>'editions',
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'order' => 'ASC'
);
?>


<!-- PAGERBOARD -->
<div class="wrapper">

  <ul class="editions">

  <?php


    $q = new WP_Query($args);
    $i = 0;

    while ( $q->have_posts() ) : $q->the_post();
      if (isset($post)): ?>

         <li class="<?php echo ($i == 0) ? 'active': '' ?>">
          <a class="editions--link" data-edition-id="<?php echo $post->ID ?>" href>
            <div class="yellow"></div>
            <div class="black"></div>
            <div>N°&nbsp;<?php echo get_field('edition_number') ?></div>
          </a>
        </li>        

      <?php 
      endif;
    endwhile;

    wp_reset_postdata();

    ?>
  </ul>

  <div class="pagerboard">
    <div class="pagerboard-pages">
      <ul>
        <?php 
          
          $args = array(
            'post_type' =>'editions',
            'post_status' => 'publish',
            'posts_per_page' => 1,
            'order' => 'ASC'
          );

          $q = new WP_Query($args);

          while ( $q->have_posts() ) : $q->the_post();
            if(isset($post)): ?>

              <?php
              $articles = get_field('edition_article');

              for ($i=0; $i < count($articles); $i++) { ?>

                <li data-title="<?php the_title(); ?>">

                <?php

                $image = get_field('article_image', $articles[$i]->ID);
                $video = get_field('article_video', $articles[$i]->ID);
                
                if ( isset($image) && !empty($image) ) : 

                  foreach ($image as $key => $value):
                    if ($key == "url") :
                      echo "<div class='article__image-background' style='background-image: url(". $value .")'></div>";
                    endif;
                  endforeach;

                endif;

                if ( isset($video) && !empty($video) ): 
                  echo "<div class='article__video--iframe'>" . $video . "</div>";
                endif;


                ?>

                </li>

                <?php } ?>

              

              <?php              
              // }

            endif;
          endwhile;

          wp_reset_postdata();

        ?>
      </ul>
    </div>
  </div>
</div>
<!-- / PAGERBOARD -->