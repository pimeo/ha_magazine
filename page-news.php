<?php
/*
Template Name: Page News
*/
?>

<?php get_header(); ?>

<?php get_breadcrumbs('C’est que du vrai !'); ?>

<div class="main news">
      <div class="post">
        
        <div class="post-content clearfix">

            <div class="global">
          
              <?php $my_query = new WP_Query(array('post_type' => 'news'));
              while ($my_query->have_posts()) : $my_query->the_post();
              $do_not_duplicate = $post->ID; ?>
          
                <div class="post-global">
                  <div class="article">
                    <div class="photo"><?php the_post_thumbnail('full'); ?></div>
                    <div class="contenu">
                      <div class="center">  
                        <?php the_content(); ?>
                      </div>
                      <div class="opacity"></div>
                    </div>
                  </div>
                  <div class="video">
                    <?php the_field('ajouter_une_video'); ?>
                  </div>
                </div>

              <?php endwhile; wp_reset_query();?>

                
            </div>
        </div>
      </div>
</div>

<?php get_footer(); ?>
