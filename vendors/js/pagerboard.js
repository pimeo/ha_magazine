/**	
* PAGERBOARD
* @author: pimeo
* @version: 0.1
* @date: 24/06/2013
**/


;(function($, window, document, undefined){

	// plugin Name
	var pluginName = "PagerBoard";

	// Defaults params
	var defaults = {
		pageWidth: 400,
		pageHeight: 500,
		generatePagination: true,
		generateTitleThumb: true,
		wrapperAttrs : {
			class: 'pagerboard-navigation'
		},
		wrapperThumbAttrs: {
			class: 'pagerboard-item'
		},
		selectedThumbClass: 'selected',

		// functions
		onBeforeChangingPage : 	function () {},
		onFirstEnterPagerBoard: function () {},
		onEnterPagerBoard: 		function() {},
		onFirstQuitPagerBoard: 	function () {},
		onQuitPagerBoard: 		function() {},
		onChangePage: 			function() {},
		onNavigatePage: 		function() {},
	};

	// PagerBoard instanciation
	function PagerBoard ( element, options ){

		this.el = element;
		this.$el = $(this.el);

		this.defaultOffsetTop = this.$el.offset().top;

		this.options = $.extend( {}, defaults, options );
		this._defaults = defaults;
		this._name = pluginName;


		this.currentPos = 0;
		this.selectedPos = 0;
		this.isInPagerBoard = false;

		this.onFirstEnter = false;
		this.onEnter = false;
		this.onFirstQuit = false;
		this.onQuit = false;

		this.init();
		this.events();

	};


	PagerBoard.prototype = {
		init: function(){
			var self = this;
			self.countPages = self.$el.find('ul li').length;
			self.$pages = self.$el.find('.pagerboard-pages');
			self.getPageParams();
			self.createNavigation();
			self.$nav.scrollToFixed({
				limit: self.limit - self.defaultOffsetTop - 70
			});

			console.log('limit', this.limit - this.defaultOffsetTop, this.limit, this.defaultOffsetTop)
		},

		events: function(){
			
			var self = this;

			$(window).on('scroll.ScrollPagerBoard', function(){
				self.manageNavigation();
			});

			self.$el.on('pagerBoard.onFirstEnter', self.options.onFirstEnterPagerBoard).bind(self);
			self.$el.on('pagerBoard.onEnter', self.options.onEnterPagerBoard).bind(self);

			self.$el.on('pagerBoard.onFirstQuit', self.options.onFirstQuitPagerBoard).bind(self);
			self.$el.on('pagerBoard.onQuit', self.options.onQuitPagerBoard).bind(self);

			self.$el.on('pagerBoard.onChangePage', self.options.onChangePage).bind(self);

			// specific to navigation items
			self.$navList.children().on('click.onNavigatePage', function(e){
				if(e) (e.preventDefault) ? e.preventDefault() : e.returnValue = false;
				self.navigateToPage(this);
			}).bind(self);

		},

		bind: function(scope){
			if (typeof this !== "function") {
		      // closest thing possible to the ECMAScript 5 internal IsCallable function
		      throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
		    }

		    var aArgs = Array.prototype.slice.call(arguments, 1), 
		        fToBind = this, 
		        fNOP = function () {},
		        fBound = function () {
		          return fToBind.apply(this instanceof fNOP && oThis
		                                 ? this
		                                 : oThis,
		                               aArgs.concat(Array.prototype.slice.call(arguments)));
		        };

		    fNOP.prototype = this.prototype;
		    fBound.prototype = new fNOP();

		    return fBound;
		},

		navigateToPage : function(elem){

			itemOffset 		= parseInt($(elem).attr('data-offset'));

			$('body').animate({scrollTop: this.defaultOffsetTop + itemOffset}, 2000);
		},

		getPageParams: function(){
			this.page = {};
			this.page.pageWidth = this.$pages.find('ul li:first').width();
			this.page.pageHeight = this.$pages.find('ul li:first').height();

			this.page.pageOuterWidth = this.$pages.find('ul li:first').outerWidth(true);
			this.page.pageOuterHeight = this.$pages.find('ul li:first').outerHeight(true);

			this.limit = this.$pages.offset().top + this.$pages.height();

			console.log(this.$pages.find('li:first').height());
		},

		createNavigation: function(){	

			var self = this;		
			// creation des divs
			this.$nav = $('<div/>')
						.attr( this.options.wrapperAttrs )
						.appendTo( this.$el );

			this.$navContainer = $('<div/>', {class: 'pagerboard-container'}).appendTo(this.$nav);
			this.$navList = $('<ul/>').appendTo(this.$navContainer);

			// creation des thumbs
			for (var i = 1;  i <= this.countPages; i++) {
				var thumb = $('<li/>').attr(this.options.wrapperThumbAttrs).append($('<a/>')),
					thumbTitle =  this.$el.find('ul li').eq(i).data('title');
				
				thumb.attr({
					'data-id': i,
					title: thumbTitle,
					//'data-offset': 
				});

				thumb.find('a').attr({
					href: '#page_' + i
				});

				// ( ((i-1) & 1 ) == 1 ) ? (i-1) * self.page.pageOuterHeight : (i-2) * self.page.pageOuterHeight
				this.$navList.append(thumb);
			};

		},

		manageNavigation: function(){
			var self = this,
				posY = 0,
				y = $(window).scrollTop();

			// if offset in hover the pagerboard div
			if( y > self.defaultOffsetTop && y < self.limit ){
				posY = y - self.defaultOffsetTop;

				//console.log(y, posY, self.limit, self.defaultOffsetTop)

				//var t = self.page.pageHeight % posY ;
				//var t2 =  posY % self.page.pageHeight;
				var u = ((posY - posY % self.page.pageOuterHeight) / self.page.pageOuterHeight) + 1;
				//var v =  ((posY % self.page.pageHeight) + self.page.pageHeight) % self.page.pageHeight
				//var f = Math.floor(self.$el.height() / posY ); 
				//console.log('index ', u );

				if( u == self.currentPos ) return;
				if( !self.currentPos ) self.currentPos = true;
				

				if( self.onEnter ){
					self.$el.trigger('pagerBoard.onChangePage');
				}

				if( self.onFirstEnter && !self.onEnter ){
					self.onQuit 		= false;
					self.onEnter 		= true;
					self.isInPagerBoard = true;
					self.$el.trigger('pagerBoard.onEnter');	
				} 

				if( !self.onFirstEnter ){
					self.onQuit 		= false;
					self.onEnter 		= true;
					self.onFirstEnter 	= true;
					self.isInPagerBoard = true;
					self.$el.trigger('pagerBoard.onFirstEnter');
				}

				self.$navList.find('li a').removeClass(self.options.selectedThumbClass);
				self.$navList.find('li:eq('+ (2*u - 1) +') a').addClass(self.options.selectedThumbClass);
				self.$navList.find('li:eq('+ (2*u - 2) +') a').addClass(self.options.selectedThumbClass);

				this.currentPos = u;

			}else{
				if( this.isInPagerBoard ){

					if( self.onFirstQuit && !self.onQuit ){
						self.onQuit 	= true;
						self.onEnter 	= false;
						self.$el.trigger('pagerBoard.onQuit');	
					} 

					if( !self.onFirstQuit ){
						self.onFirstQuit 	= true;
						self.onQuit 		= true;
						self.onEnter 		= false;
						self.$el.trigger('pagerBoard.onFirstQuit');
					}

					self.$navList.find('li a').removeClass(self.options.selectedThumbClass);
					self.currentPos = 0;
					self.isInPagerBoard = false;
				}
			}
		},

		deb: function(el, str){
			if( str ) console.log(str, el);
			else console.log(el);
		}
	};


	$.fn[pluginName] = function( options ){
		return this.each(function(){
			if( !$.data(this, "plugin_" + pluginName) ){
				$.data(this, "plugin_" + pluginName, new PagerBoard(this, options));
			}
		})
	}

}(jQuery, window, document));