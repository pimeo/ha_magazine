(function($){

		$(document).on('ready', init);


		// init
		function init(e){


			// $('.menu li a').hover(function(){
			// 	$(this).find('.black').stop().fadeIn(200);
			// }, function(){
			// 	$(this).find('.black').stop().fadeOut(200);
			// });

			$(window).on('resize', resize);

			function resize(){
				// breadcrumbs
				$('.breadcrumbs .line').css('width', function(){
					return ( ( $(window).width() - $('.breadcrumbs-container').width() ) / 2 ) + 580;
				});
			}

			resize();

			$('.pagerboard').PagerBoard({
				onBeforeChangingPage : 	function () {
					console.log('PB :: onBeforeChangingPage');
				},
				onFirstEnterPagerBoard: function () {
					console.log('PB :: onFirstEnterPagerBoard');
				},
				onEnterPagerBoard: 		function() {
					console.log('PB :: onEnterPagerBoard');
				},
				onFirstQuitPagerBoard: 	function () {
					console.log('PB :: onFirstQuitPagerBoard')
				},
				onQuitPagerBoard: 		function() {
					console.log('PB :: onQuitPagerBoard')
				},
				onChangePage: 			function() {
					console.log('PB :: onChangingPage')
				}
			});

			$('.editions').scrollToFixed({
				marginTop: 40,
			});

		}

}(jQuery));


