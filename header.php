<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head <?php language_attributes(); ?>>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">

  <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/style.min.css" type="text/css">
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/jquery.mCustomScrollbar.css" type="text/css">
  
  <!-- PREVENIR CONTRE LES BUGS DES DIFFERENTS NAVIGATEURS -->
  <script type="text/javascript" src="<?php bloginfo('template_directory') ?>/vendors/js/modernizr-2.6.2.min.js"></script>


  <?php wp_head(); ?>

</head>

<body <?php body_class(); ?> >

  <!-- LAYOUT -->
  <div class="layout">
  
    <!-- HEADER -->
    <header id="header">
      <div class="header-container clearfix">
      <!-- NAVIGATION -->
        <nav class="navigation">
          <ul class="menu">
            <li class="long">
              <a href="<?php echo bloginfo('url') ?>" class="active">
                <span>Sondage de l'univers</span>
                <div class="black--rect"></div>
                <div class="black--shadow"></div>
              </a>
            </li>
            <li class="short">
              <a href="<?php echo get_permalink(71) ?>">
                <span>Todo List</span>
                <div class="black--rect"></div>
                <div class="black--shadow"></div>
              </a>
            </li>
            <li class="short">
              <a href="<?php echo get_permalink(66) ?>#">
                <span>Le Magazine</span>
                <div class="black--rect"></div>
                <div class="black--shadow"></div>
              </a>
            </li>
            <li class="short">
              <a href="<?php echo get_permalink(167) ?>">
                <span>News</span>
                <div class="black--rect"></div>
                <div class="black--shadow"></div>
              </a>
            </li>
            <li class="short">
              <a href="<?php echo get_permalink(66) ?>">
                <span>Buy Online</span>
                <div class="black--rect"></div>
                <div class="black--shadow"></div>
              </a>
            </li>
          </ul>
        </nav>
        <!-- / NAVIGATION -->

        <!-- LOGO -->
        <a class="logo" href="<?php bloginfo('url') ?>">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="207.811px" height="207.811px" viewBox="0 0 207.811 207.811" enable-background="new 0 0 207.811 207.811" xml:space="preserve">
            <rect x="21.23" y="21.231" fill="#2B2B2B" width="186.58" height="186.578"/>
            <rect x="0.5" y="0.5" fill="#FFFFFF" stroke="#231F20" stroke-miterlimit="10" width="186.578" height="186.578"/>
            <path fill="#000100" d="M21.529,122.803V58.106H34.96v24.667h25.15V58.106h13.431v64.698H60.111V93.922H34.96v28.881H21.529z"/>
            <path fill="#000100" d="M105.034,58.106h15.297l22.89,64.698h-14.66l-4.269-13.299h-23.826l-4.386,13.299H81.94L105.034,58.106z
               M104.144,98.355h16.569l-8.173-25.458L104.144,98.355z"/>
            <path fill="#000100" d="M154.48,58.325h13.738v16.4l-3.556,30.17h-6.54l-3.643-30.17V58.325z M154.786,110.118h13.124v12.686
              h-13.124V110.118z"/>
          </svg>
        </a>
        <!-- / LOGO -->

        <!-- TOP RIGHT MENU -->
        <ul class="secondary-links">
          <li>
            <a href="<?php echo get_permalink(209) ?>"><?php echo get_the_title(209) ?></a>
          </li>
          <li class="sep"></li>
          <li>
            <a href="<?php echo get_permalink() ?>"><?php echo get_the_title(211) ?></a>
          </li>
        </ul>
        <!-- TOP RIGHT MENU -->

      </div>

    </header>
    <!-- / HEADER -->
