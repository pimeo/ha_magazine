'use strict';
module.exports = function(grunt) {

    // load all grunt tasks matching the `grunt-*` pattern
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        // watch for changes and trigger sass, jshint, uglify and livereload
        watch: {
            sass: {
                files: ['assets/styles/**/*.{scss,sass}'],
                tasks: ['sass', 'autoprefixer', 'cssmin']
            },
            js: {
                files: '<%= jshint.all %>',
                tasks: ['jshint', 'uglify']
            },
            livereload: {
                options: { livereload: true },
                files: ['style.css', 'build/js/*.js', 'assets/scripts/*.coffee', 'assets/images/**/*.{png,jpg,jpeg,gif,webp,svg}']
            },
            coffee: {
              files: ['assets/scripts/{,*/}*.coffee', 'assets/scripts/**/{,*/}*.coffee'],
              tasks: ['coffee:dist', 'uglify:main']
            }
        },

        // sass
        sass: {
            dist: {
                options: {
                    sourcemap: false,
                    style: 'compressed',
                    compass: true
                },
                files: {
                    'build/css/style.css': 'assets/styles/style.scss',
                    'build/css/editor-style.css': 'assets/styles/editor-style.scss'
                }
            }
        },

        // autoprefixer
        autoprefixer: {
            options: {
                browsers: ['last 2 versions', 'ie 9', 'ios 6', 'android 4'],
                map: false
            },
            files: {
                expand: true,
                flatten: true,
                src: 'build/css/*.css',
                dest: 'build/css'
            },
        },

        // coffee
        coffee: {
          options: {
            sourceMap: false,
            sourceRoot: '',
            bare: true
          },
          dist: {
            files: [{
              expand: true,
              cwd: 'assets/scripts',
              src: ['**/*.coffee'],
              dest: 'build/js',
              ext: '.js',
              options: {
                bare: true,
                preserve_dirs: true
              }
            }]
          }
        },

        // css minify
        cssmin: {
            options: {
                keepSpecialComments: 1
            },
            minify: {
                expand: true,
                cwd: 'build/css',
                src: ['*.css', '!*.min.css'],
                ext: '.min.css',
                dest: 'css'
            }
        },

        // javascript linting with jshint
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                "force": true
            },
            all: [
                'Gruntfile.js',
                'assets/scripts/**/*.js'
            ]
        },

        // uglify to concat, minify, and make source maps
        uglify: {
            plugins: {
                options: {
                    sourceMap: false
                    // sourceMap: 'js/plugins.js.map',
                    // sourceMappingURL: 'plugins.js.map',
                    // sourceMapPrefix: 2
                },
                // all vendors here
                files: {
                    'js/plugins.min.js': [
                        'vendors/js/jquery-1.9.1.min.js',
                        'vendors/js/scrollToFixed.js',
                        'vendors/js/pagerboard.js',
                        'vendors/js/jquery.mCustomScrollbar.min.js',
                        // 'assets/js/source/plugins.js',
                        // 'assets/js/vendor/skip-link-focus-fix.js',
                        // 'assets/js/vendor/yourplugin/yourplugin.js',
                    ]
                }
            },
            main: {
                options: {
                    sourceMap: false
                    // sourceMap: 'js/app.js.map',
                    // sourceMappingURL: 'app.js.map',
                    // sourceMapPrefix: 2
                },
                files: {
                    'js/app.min.js': [
                        'build/js/*.js',
                        'build/js/**/*.js'
                    ]
                }
            }
        },

        // image optimization
        imagemin: {
            dist: {
                options: {
                    optimizationLevel: 7,
                    progressive: true,
                    interlaced: true
                },
                files: [{
                    expand: true,
                    cwd: 'assets/images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'assets/images/'
                }]
            }
        }
    });

    // register task
    grunt.registerTask('default', [
      'sass', 
      'coffee:dist',
      'autoprefixer', 
      'cssmin', 
      'uglify', 
      'watch'
    ]);

    // build
    grunt.registerTask('build', [
        
    ]);

};