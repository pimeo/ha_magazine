    <!-- FOOTER -->
    <div id="footer">
      <div class="footer-container clearfix">
        <div class="main buy">
          <?php $my_query = new WP_Query(array('post_type' => 'page', 'p' => 66 ));
                    while ($my_query->have_posts()) : $my_query->the_post();
                  $do_not_duplicate = $post->ID; ?>
              <div class="post">
                
                <div class="post-content clearfix">
                    <div id="block-left" class="block left">  
                      <div class="block-center">
                      <div class="text-center">
                        <?php the_content(); ?>
                      </div>
                      </div>
                      <div class="border-black">
                      </div>
              </div>
              <div id="block-right" class="block right">
                  <div class="block-center">
                    <div class="title">
                      <h2>Commander</h2>
                      <span class="block1"></span>
                      <span class="block2"></span>
                    </div>
                    <?php echo do_shortcode( '[gravityform id="2" name="Commander" title="false" description="false"]' ) ?>
                  </div>
                  <div class="border-black">
                  </div>
              </div>
                </div>
              </div>

              <?php endwhile; wp_reset_query();?>

          <div id="footer-buy">
            <ul class="clearfix">
              <li class="contact">
                <?php $my_query = new WP_Query(array('post_type' => 'footeracheter', 'p' => 121 ));
                    while ($my_query->have_posts()) : $my_query->the_post();
                  $do_not_duplicate = $post->ID; ?>
                  
                  <h3><img src="<?php bloginfo('template_directory'); ?>/img/logo_petit.png" alt="logo-ha"><?php the_title(); ?></h3>
                    <p><?php the_content(); ?></p>
                    <p><?php the_field('telephone'); ?></p>
                    <p><?php the_field('mail'); ?></p>


                  <?php endwhile; wp_reset_query();?>
              </li>
              <li class="equipe">
                <?php $my_query = new WP_Query(array('post_type' => 'footeracheter', 'p' => 127 ));
                    while ($my_query->have_posts()) : $my_query->the_post();
                  $do_not_duplicate = $post->ID; ?>
                  
                  <h3><img src="<?php bloginfo('template_directory'); ?>/img/logo_petit.png" alt="logo-ha"><?php the_title(); ?></h3>
                    <p><?php the_content(); ?></p>

                  <?php endwhile; wp_reset_query();?>
              </li>
              <li class="welcome">
                <?php $my_query = new WP_Query(array('post_type' => 'footeracheter', 'p' => 132 ));
                    while ($my_query->have_posts()) : $my_query->the_post();
                  $do_not_duplicate = $post->ID; ?>
                  
                  <h3><img src="<?php bloginfo('template_directory'); ?>/img/logo_petit.png" alt="logo-ha"><?php the_title(); ?></h3>
                    <p><?php the_content(); ?></p>

                  <?php endwhile; wp_reset_query();?>
              </li>
            </ul>
          </div>

        </div>
      </div>
    </div>
    <!-- / FOOTER -->
  </div>
  <!-- / LAYOUT -->

  <script src="<?php bloginfo('template_directory') ?>/js/plugins.min.js"></script>
  <script src="<?php bloginfo('template_directory') ?>/js/app.min.js"></script>
  
  <?php wp_footer() ?>
  

  </body>
</html>

