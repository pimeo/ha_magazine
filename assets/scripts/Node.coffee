class Node
  constructor: ->
 
  find: (classname, elem = null) ->
    e = elem || document
    e.querySelector(classname)

  findTag: (tag, elem) ->
    e = elem || document
    e.getElementsByTagName(tag)[0]

  findAll: (classname, elem = null) ->
    e = elem || document
    e.querySelectorAll(classname)

  findAttr: (attr = null, elem = null) ->
    elem.getAttribute(attr) if elem?