class Survey
  constructor: ->
    @datas = null

  load: ->
    $.getJSON("#{rootPath}ajax-survey.php")
     .done @successRequest
     .fail @errorRequest

  successRequest: (result) =>
    if result?
      @datas = result 
      
      console.log @datas

      # ---
      # ensemble des données que retourne l'objet
      # ---
      # @datas.survey -> informations globale sur le sondage
      # @datas.questions -> listing des question
        # @datas.questions[x] -> objet question à l'index x
          # @datas.questions[x].answer_categories -> les catégories des réponses rand obtenues (meme ordre que le tableau de rep)
          # @datas.questions[x].answer_categories -> tableau de cpt réponses random
          # @datas.questions[x].question -> cpt question liée à notre sondage (ordre établi par le filtre d'ordre du sondage).



  errorRequest: (data) =>
    console.log "error"



  # 1. je créé une classe qui va gérer l'ensemble de mes questions
  # 2. chaque question comporte 4 réponses dont une générée tout le temps "ha"
  # 3. a chaque question selectionnée, la classe qui va gérer l'ensemble de mes questions
  # doit être informé puis elle doit elle-meme passer a la question suivante
  # tant que le sondage n'est pas arrivé aux termes du nombre de questions prévue dans la variable @datas
  # 4. a la fin de mon sondage, je dois obtenir une interface avec un diagramme, et une liste de réponses (je suis blabla ou je suis blibli)
  # qui seront informé dans des champs personnalisé propre au sondage en cours.
  # - un seul sondage a la fois. seul le sondage publié et le plus récent est affiché.




  class MonSondage
    constructor: ->
      @questions = []
      @currentQuestionID = 0
      @answers = []
      for i in [0...@datas.questions.length]
        q = new MaQuestion @datas.questions[i], @onQuestionSelected
        @questions.push q

      onQuestionSelected: (questionID, answer) =>
        console.log 'reponse pour questions', questionID, answer
        # je sauvegarde ma réponse dans mon tableau answers
        @answers[questionID] = answerID

        # je peux cacher ma question
        @questions[@currentQuestionID].hide()
        # je passe a la questions suivante
        @currentQuestionID = questionID+1
        # je change de questio
        @nextQuestion()

      nextQuestion: ->
        # j'affiche ma questions suivante
        # @questions[@currentQuestionID].show()


  class MaQuestion
    constructor: (question, questionCallback)->
      @id = question.ID
      @answer = null
      @answerSelected = null

      # ajouter les responses dans le dom de facon dynamique
      # cacher par defaut l'ensemble des questions
      # elles seront activés par la classe MonSondage

      # clic sur ces fameuses réponses
      @responses = document.querySelectorAll '.mes-reponses a'
      for response in @responses
        response.addEventListener 'click', @onClickAnswer, false

    # des evenement clic detecté, on définit a la classe MonSondage 
    # de la selection de la question 
    onClickAnswer: (e) =>
      e?.preventDefault()
      # exemple : je récupere la valeur dans l'attribut name qui comporte un element de ma reponse
      # a toi de définir si tu souhaite récupérer un id de catégories ou un slug catégories...
      questionCallback?(@id, e.currentTarget.getAttribute 'name')

    show: ->

    hide: ->





