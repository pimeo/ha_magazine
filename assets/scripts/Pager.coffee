rootPath = "/wp-content/themes/ha_magazine/ajax/"


class Pager extends Node
  constructor: () ->
    super()

    @editions = @findAll ".editions li"
    for link in @editions
      link.addEventListener "click", @onSelectEdition, false

    $(".pagerboard").PagerBoard() if @editions.length


  onSelectEdition: (e) =>
    e?.preventDefault()
    console.log "edition id", e.currentTarget.getAttribute("data-edition-id")
    @requestEdtition e.currentTarget.getAttribute("data-edition-id")

  requestEdtition: (id) ->
    $.ajax
      method: "get"
      format: "html"
      url: "#{rootPath}ajax-pagerboard.php"
      data:
        p: id
      success: @selectCallback
      error: @selectCallback


  selectCallback: (xhr, data, status) =>
    console.log "callback", data