<?php
/*
Template Name: Page to do list
*/
?>

<?php get_header(); ?>

<?php get_breadcrumbs('Et vous, ça va ?'); ?>

<div class="main todo">
      <div class="post">
        
        <div class="post-content clearfix">

          	<div class="titre clearfix">
          		<img src="<?php bloginfo('template_directory'); ?>/img/logo_petit.png" alt="logo-ha">
          		<h2>Share you everyday imagination</h2>
          	</div>
            
            <div class="global">
              <div class="cadre-todo">
                <div class="cadre-white">
                <?php

                  // The Query
                  $the_query = new WP_Query( array('post_type' => 'todolist') );

                  // The Loop
                  if ( $the_query->have_posts() ) {
                    echo '<ul id="content-todo">';
                    while ( $the_query->have_posts() ) {
                      $the_query->the_post();
                      echo '<li><span>' . get_the_content() . '</span><span class="check">'. get_field('validation') . '</span></li>';
                    }
                    echo '</ul>';
                  } else {
                    // no posts found
                  }
                  /* Restore original Post Data */
                  wp_reset_postdata(); ?>

                <?php if (have_posts()) : ?>
                  <?php while (have_posts()) : the_post(); ?>
                    <?php the_content(); ?>
                  <?php endwhile; ?>
                <?php endif; wp_reset_query(); ?>
                </div>
                <div class="cadre-black">
                  
                </div>
              </div>
              <div class="block-citation">
                <ul>
                  <li>“EXPRESSION IS LIFE, LIFE IS SUCESS”</li>
                  <li>Mélodie de HA! Magazine</li>
                </ul>
              </div>
            </div>
        </div>
      </div>
</div>

<?php get_footer(); ?>
