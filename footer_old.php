    <!-- FOOTER -->
    <div id="footer">
      <div class="footer-container clearfix">
        <div class="blocks clearfix">


          <!-- BLOC INFOS HA -->
          <div class="block yellow">
            <p>Lorem ipsum Adipisicing fugiat minim ullamco ex proident nisi velit esse dolor dolore sit laborum commodo.</p>
            <a href="#">Some text here <b class="arrow"></b> </a>
          </div>
          <!-- / BLOC INFOS HA -->

          <!-- BLOC ACHATS MAG -->
          <div class="block black">
            <h4>Ha Magazine !</h4>
            <form>
              <div class="form-group">
                <label>Magazine</label>
                 <div class="input-control">
                   <select>
                    <option>Numéro 3 : Pavannes ses privilèges</option>
                    <option>Numéro 2 : Tartiner le cendrier</option>
                    <option>Numéro 2 : Balayer avec l'aspirateur</option>
                  </select>
                 </div>
              </div>
              <div class="form-group">
                <label>Prix</label>
                <span class="prices">
                    <span class="price">8.50</span> euros
                </span>
              </div>
              <div class="form-group">
                <label>Quantité</label>
                <div class="input-control">
                    <input type="number" name="howmuch">
                </div>
              </div>
              <div class="form-group">
                <label>Email</label>
                <div class="input-control">
                    <input type="email">
                </div>
              </div>
               <div class="form-group">
                <label>Prénom / Nom</label>
                <div class="input-control">
                    <input type="text">
                </div>
              </div>
              <div class="form-group">
                <label>Adresse</label>
                <div class="input-control">
                    <input type="text">
                </div>
              </div>
               <div class="form-group">
                <label>Code Postal</label>
                <div class="input-control">
                    <input type="text">
                </div>
              </div>
              <div class="form-group">
                <label>Pays/Country</label>
                <div class="input-control">
                    <input type="text">
                </div>
              </div>
              <div class="form-group form-submit clearfix">
                <button class="custom-btn">Acheter sans compte Paypal</button>
                <button class="custom-btn">Acheter avec Paypal</button>
              </div>

              <div class="form-group infos">
                <span>* Toutes les informations restent strictement confidentielles</span>
              </div>
            </form>
          </div>
          <!-- BLOC ACHATS MAG -->

        </div>
        <!-- COPYRIGHT -->
        <div class="copyright">© Chromatiquementvotre Production - 2013</div>
        <!-- / COPYRIGHT -->
        
      </div>
    </div>
    <!-- / FOOTER -->

  </div>
  <!-- / LAYOUT -->

  <script src="<?php bloginfo('template_directory') ?>/js/plugins.min.js"></script>
  <script src="<?php bloginfo('template_directory') ?>/js/app.min.js"></script>
  <script src="<?php bloginfo('template_directory') ?>/js/jquery.mCustomScrollbar.min.js"></script>

  <?php wp_footer() ?>

  </body>
</html>