<?php
if ( !function_exists('init_configurations')){

	function init_configurations()
	{
	
		// Creation d'un menu
		// Utiliser la fonction wp_nav_menu().
		register_nav_menus(
			array(
				// Menu principal
				'header-menu', 'Menu principal',
				// Menu du pied de page
				'footer-menu', 'Menu de pied de page'
			) 
		);

		// Utilisation des miniatures par défaut
		add_theme_support( 'post-thumbnails' );
		// Largeur 150, Hauteur (peu importe), recadrage soft (argument non passé mais par défaut à false)
		set_post_thumbnail_size( 150, 9999 ); 
	}

}

// On lance la fonction init_configurations après l'initialisation des configurations du theme wordpress courant
add_action('after_setup_theme', 'init_configurations');


function get_breadcrumbs($name){
	$str = file_get_contents(get_template_directory() . '/partials/breadcrumbs.php');
	$str = str_replace('%NAME%', $name, $str);
	echo $str;
}


//

add_filter("gform_post_data", "change_post_status");     

function change_post_status($post_data, $form){

    //only change post status on form id 5

    if($form["id"] != 0)

       return $post_data;

 

    $post_data["post_status"] = "publish";

    return $post_data;

}


add_filter("gform_init_scripts_footer", "init_scripts");
	function init_scripts() {
		return true;
}