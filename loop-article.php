<?php 
if (have_posts()) :
  while (have_posts()) : the_post(); 

  ?>

    <li id="<?php the_ID(); ?>" data-title="<?php the_title(); ?>" <?php post_class(); ?>>
      <?php the_content(); ?>
    </li>

  <?php endwhile; ?>

<?php endif; ?>
