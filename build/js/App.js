var App, app;

App = (function() {
  function App() {
    console.log('[App] init coucou');
    this.survey = new Survey();
    this.survey.load();
    if ($(".center").length) {
      $(".center").mCustomScrollbar();
    }
  }

  if ($("#content-todo").length) {
    $("#content-todo").mCustomScrollbar();
  }

  return App;

})();

app = null;

$(document).ready((function(_this) {
  return function() {
    return app = new App();
  };
})(this));

jQuery(document).ready(function() {
  jQuery(".video").each(function() {
    var $self;
    $self = jQuery(this);
    if ($self.children().length === 0) {
      console.log("add class");
      $self.addClass("empty");
    }
  });
  jQuery(".photo").each(function() {
    var $self;
    $self = jQuery(this);
    if ($self.children().length === 0) {
      console.log("add class");
      $self.parent().addClass("empty");
    }
  });
});
