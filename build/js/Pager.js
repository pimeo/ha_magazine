var Pager, rootPath,
  __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
  __hasProp = {}.hasOwnProperty,
  __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

rootPath = "/wp-content/themes/ha_magazine/ajax/";

Pager = (function(_super) {
  __extends(Pager, _super);

  function Pager() {
    this.selectCallback = __bind(this.selectCallback, this);
    this.onSelectEdition = __bind(this.onSelectEdition, this);
    var link, _i, _len, _ref;
    Pager.__super__.constructor.call(this);
    this.editions = this.findAll(".editions li");
    _ref = this.editions;
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      link = _ref[_i];
      link.addEventListener("click", this.onSelectEdition, false);
    }
    if (this.editions.length) {
      $(".pagerboard").PagerBoard();
    }
  }

  Pager.prototype.onSelectEdition = function(e) {
    if (e != null) {
      e.preventDefault();
    }
    console.log("edition id", e.currentTarget.getAttribute("data-edition-id"));
    return this.requestEdtition(e.currentTarget.getAttribute("data-edition-id"));
  };

  Pager.prototype.requestEdtition = function(id) {
    return $.ajax({
      method: "get",
      format: "html",
      url: "" + rootPath + "ajax-pagerboard.php",
      data: {
        p: id
      },
      success: this.selectCallback,
      error: this.selectCallback
    });
  };

  Pager.prototype.selectCallback = function(xhr, data, status) {
    return console.log("callback", data);
  };

  return Pager;

})(Node);
