<?php

ini_set( 'display_errors', 'off' );

define('WP_USE_THEMES', false);  
require_once('../../../../wp-load.php');

global $wp_query;

class Survey{

  public function __construct(){
    $this->request = new stdClass();

    $this->responses               = array();
    $this->categories              = array('ou', 'comment', 'pourquoi', 'quand', 'qui', 'reponse-general', 'verbe-daction');

    foreach ($this->categories as $category) {
      $this->responses[$category] = $this->getAnswers($category);
    }

    $this->questionsArgs = array(
      'post_type'       => 'sondages',
      'post_status'     => 'publish',
      'posts_per_page'  => 1,
      'order'           => 'ASC'
    );

  }

  protected function getAnswers($category_name){
    return query_posts("post_type=reponse&showposts=10&orderby=rand&category_name=$category_name");
  }

  public function getQuestions(){
    $wp_qry = new WP_Query($this->questionsArgs);

    while ( $wp_qry->have_posts() ) : $p = $wp_qry->the_post();
      
      $p = $wp_qry->post;
      $this->request->survey    = $p;
      $this->request->questions = array();
      $questions                = get_field('relation_questions', $p->ID);

      $i = 0;
      foreach ($questions as $q) :
        $o = new stdClass;
        $o->question = $q;
        $o->answer_categories = array_rand($this->responses, 3);
        $o->anwsers = array();

        foreach ($o->answer_categories as $category) :
          $index  = array_rand($this->responses[$category], 1);
          array_push($o->anwsers, $this->responses[$category][$index]);
        endforeach;

        array_push($this->request->questions, $o);

      endforeach;

    endwhile;

    wp_reset_postdata();   

  }

  public function encode(){
    return json_encode($this->request);
  }

}

$survey = new Survey();
$survey->getQuestions();
echo $survey->encode();