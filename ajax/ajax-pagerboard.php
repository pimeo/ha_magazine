<?php

// define('WP_USE_THEMES', false);
require_once('../../../../wp-load.php');

global $wp_query;

// ALL POSTS
function allArticles(){
  $args = array(
    'posts_per_page' => -1
  );
  $p = new WP_Query($args);
  $o = new StdClass();

  $o->firstID = $p->posts[0]->ID;
  $o->lastID = $p->posts[count($p->posts)-1]->ID;
  return $o;
}

// CREATE DATAS
function createDatas($args){

  $q = new WP_Query($args);

  // retrieve posts object
  while ( $q->have_posts() ) : $q->the_post();
    if(isset($post)): ?>

      <?php
      $articles = get_field('edition_article');

      for ($i=0; $i < count($articles); $i++) : ?>

        <li data-title="<?php the_title(); ?>">


        </li>

        <?php 
      endfor;
    endif;
  endwhile;

}

// REQUEST
$request = new StdClass();
$request->meta = allArticles();
wp_reset_postdata();

// QUERY OF POST
$argsPosts = array(
  'post_type'       => 'editions',
  'post_status'     => 'publish',
  'posts_per_page'  => 1,
  'p'               => strip_tags(trim($_POST['postID']))
);

// QUERY POSTS
echo createDatas($argsPosts);
wp_reset_postdata();

// JSON DATAS
// echo json_encode($request);